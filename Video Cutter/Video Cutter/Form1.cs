﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Video_Cutter
{
    public partial class frmVideoCutter : Form
    {
        private Time videoTime;
        public frmVideoCutter()
        {
            InitializeComponent();
            videoTime = new Time();
            label1.Text = "Start : " + trackBar1.Value;
            label2.Text = "End : " + trackBar2.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            videoTime.setTime(1, 2, 40);
            OpenFileDialog file = new OpenFileDialog();

            if (file.ShowDialog() == DialogResult.OK)
            {
                string strfilename = file.InitialDirectory + file.FileName;
                txtFilename.Text = strfilename;
                Console.WriteLine(videoTime.totalSeconds());
            }
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            label2.Text = "End : " + trackBar2.Value;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label1.Text = "Start : " + trackBar1.Value;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.TextLength == 0)
            {
                //Do nothing
            }
            else
            {
                trackBar1.Maximum = Convert.ToInt32(textBox1.Text);
                trackBar2.Maximum = Convert.ToInt32(textBox1.Text);
            }
        }
    }
}
