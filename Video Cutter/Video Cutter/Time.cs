﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Video_Cutter
{
    
    class Time
    {
        private int hours;
        private int minutes;
        private int seconds;


        public Time()
        {

        }

        public void setTime(int h, int min, int sec)
        {
            hours = h;
            minutes = min;
            seconds = sec;
        }

        public int totalSeconds()
        {
            return (hours * 3600) + (minutes * 60) + seconds;
        }
    }
}
